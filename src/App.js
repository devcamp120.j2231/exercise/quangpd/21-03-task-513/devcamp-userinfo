import { gUserInfo } from "./info.js";
import image from "./assets/images/avatardefault_92824.png"


function App() {
  return (
    <div>
      <h5>Họ và tên : {gUserInfo.firstname}  {gUserInfo.lastname}</h5>
      <img src = {image} width="300" />
      <p>Tuổi: {gUserInfo.age}</p>
      <p>{gUserInfo.age > 35 ? "Anh Ấy Đã Già" : "Anh Ấy Còn Trẻ"}</p>
      <ul>
        {gUserInfo.language.map((item,index) =>{
          return <li key={index}>{item}</li>
        })}
      </ul>
    </div>
  );
}

export default App;
